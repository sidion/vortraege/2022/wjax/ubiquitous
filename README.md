# ubiquitous

Dies ist die Ablage für die W-JAX 2022 Session: "Neue Programmiersprache „Ubiquitous“ – Ein Domain-Driven Design (DDD) Projekt-Erfahrungsbericht".

Ihr findet hier die Folien zum Vortrag und die Excalidraw Datei.

## Escalidraw Datei
Um die Escalidraw Datei einsehen zu können geht auf folgende URL:
 * Escalidraw: https://excalidraw.com/
 und ziet die Datei per drag-and-drop dort rein.